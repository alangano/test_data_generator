#!/usr/bin/env ruby
$VERBOSE = true

require 'date'
require 'pathname'
APP_ROOT = Pathname.new(__FILE__).realdirpath.dirname.dirname
$LOAD_PATH.unshift "#{APP_ROOT}/lib"

require 'ajg/tdg'

class TestData < AJG::TDG::Generator
   def initialize
      super
      add_file(:first_name,'first_names.txt')
      add_file(:last_name,'last_names.txt')
      add_file(:street_names,'street_names_single.txt')
      add_file(:street_directions,'street_directions.txt')
      add_file(:street_suffixes,'street_suffixes.txt')
      add_file(:city,'city_names.txt')
      add_file(:state,'states.txt')
      add_file(:interests,'interests.txt')
      add_file(:random_strings,'random_strings.txt')
   end
   def first_name
      random_from_file(:first_name)
   end
   def last_name
      random_from_file(:last_name)
   end
   def age
      rand(18..70)
   end
   def address_line1
      [
         rand(10..99999),
         (rand(10) > 1 ? random_from_file(:street_directions) : ''),
         random_from_file(:street_names),
         random_from_file(:street_suffixes),
      ].join(' ')
   end
   def city
      random_from_file(:city)
   end
   def state
      random_from_file(:state)
   end
   def zip
      rand(10) < 4 ? 
         "#{rand(10000..99999)}-#{rand(1000..9999)}" :
         "#{rand(10000..99999)}"
   end
   def birth_date
      @birth_date ||= ((Date.today - (365*100))..Date.today).to_a
      @birth_date[rand(@birth_date.count)].strftime('%Y-%m-%d')
   end
   def create_date
      @create_date ||= ((Date.today - (365*10))..Date.today).to_a
      @create_date[rand(@create_date.count)].strftime('%Y-%m-%d')
   end
   def last_update_date
      @last_update_date ||= ((Date.today - (365*5))..Date.today).to_a
      @last_update_date[rand(@last_update_date.count)].strftime('%Y-%m-%d')
   end
   def termination_date
      @termination_date ||= ((Date.today - (365*8))..Date.today).to_a
      rand(100) > 70 ?
         @termination_date[rand(@termination_date.count)].strftime('%Y-%m-%d') :
         nil
   end
   def id_sequence
      @id ||= 0
      @id += 1
   end
   def status
      rand(3)+1
   end
   def disposition
      @disposition ||= [
         'SINGLE',
         'MARRIED',
         'DIVORCED',
         'WIDOWED/ER',
      ]
      @disposition[rand(@disposition.count)]
   end
   def rand_string(i_start,i_end)
      @rand_string ||= ('a'..'z').to_a + (0..9).to_a
      rand(i_start..i_end).times.
         collect { @rand_string[rand(@rand_string.count)] }.join
   end
   def rand_string2(i_start,i_end)
      @rand_string ||= ('a'..'z').to_a + (0..9).to_a + ['.','-']
      rand(i_start..i_end).times.
         collect { @rand_string[rand(@rand_string.count)] }.join
   end
   def sent_url
      @sent_url_suffix = %w{html txt php jsp}
      t_out = "http://#{rand_string(5,20)}.com/"
      t_add = rand(8)
      t_out << t_add.times.collect { "#{rand_string(1,12)}" }.join('/') +
         rand(8).times.collect { "#{rand_string(1,12)}" }.join('/') 
      t_add > 0 and
         t_out << ".#{@sent_url_suffix[rand(@sent_url_suffix.count)]}"
      t_out
   end
   def email
      @email_dns = %w{gmail.com hotmail.com aol.com yahoo.com}
      rand(100) <= 70 ?
         "#{rand_string2(5,20)}@#{@email_dns[rand(@email_dns.count)]}" :
         nil
   end
   def phone_number
      3.times.collect { rand(1..9) }.join + '-' +
         3.times.collect { rand(1..9) }.join + '-' +
         4.times.collect { rand(1..9) }.join
   end
   def home_phone
      rand(100) < 70 ?  phone_number : nil
   end
   def cell_phone
      rand(100) < 50 ?  phone_number : nil
   end
   def work_phone
      rand(100) < 30 ?  phone_number : nil
   end
   def interests
      rand(100) < 50 ?
         rand(1..3).times.collect { random_from_file(:interests) }.join(';') :
         nil
   end
   def emit(i_count=1)
      Enumerator.new { |e|
         i_count.times {
            e.yield [
               id_sequence,
               first_name,
               last_name,
               age,
               address_line1,
               city,
               state,
               zip,
               birth_date,
               email,
               home_phone,
               cell_phone,
               work_phone,
               status,
               disposition,
               sent_url,
               create_date,
               last_update_date,
               termination_date,
               interests,
            ]
         }
      }
   end
end

TestData.new .emit(10).each { |r|
   puts r.collect { |c|
      c.kind_of?(String) ?  %Q{"#{c.gsub(/"/,'""')}"} : c
   }.join(',')
}

