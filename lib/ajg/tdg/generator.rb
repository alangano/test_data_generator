
module AJG
module TDG

class Generator
   def initialize
      @files = {}
   end
   def random_from_file(i_name)
      @files[i_name].emit_random
   end
   def add_file(i_name,i_file)
      (var_dir + i_file).exist? or raise "file '#{i_file}' does not exist"
      @files.has_key?(i_name) and raise "name '#{i_name}' already declared"
      @files[i_name] = Datafile.new(var_dir + i_file)
   end
   def var_dir
      Pathname.new(APP_ROOT) / 'var' / 'ajg' / 'tdg'
   end
end

end # module TDG
end # module AJG


