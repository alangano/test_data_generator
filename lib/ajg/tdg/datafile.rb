
require 'pathname'

module AJG
module TDG

class Datafile
   attr_accessor :file
   attr_accessor :data
   def initialize(i_file)
      self.file = Pathname.new(i_file)
      file.exist? or raise "file '#{file}' does not exist"

      self.data = []
      file.open('r') { |f| f.each_line { |line| data << line.chomp } }
   end
   def emit_random
      data[rand(data.count)]
   end
end

end # module TDG
end # module AJG


