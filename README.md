
# Test Data Generator

A ruby utility that emits test data per your Ruby code.

Included is parsed out publically available data (first names, last names, street names, etc).

# Examples

bin/gen_test_data.rb is the example

Copy/edit it to generate data per your specs, and in the output format you desire.

